package grades;

/**
 * This the application for running the grade processor. Do not change the code in this class unless otherwise specified.
 * 
 * @author Yu-Cheng Tu
 *
 */
public class GradeProcessorApp {

	public static void main(String[] args) {
		System.out.println("Welcome to Course Grade Processor");
		System.out.println("=================================");
		System.out.println();
		System.out.println("Grade Summary");
		System.out.println("--------------------------");

		// You may change the string to test your code for importing the given csv file
		CourseSummary c = new CourseSummary("courseGrades.csv");
		
		c.printStudentAverage();
		c.printCountByHonourClass(HonourClass.FIRST);
		c.printCountByHonourClass(HonourClass.SECOND_FIRST);
		c.printCountByHonourClass(HonourClass.SECOND_SECOND);
		
		System.out.println("--------------------------");
		System.out.println();
		
		System.out.println("The first 15 students are: ");
		System.out.println("--------------------------");
		c.printFirst15InOrder();

	}

}
