package grades;

/**
 * This class is used for storing each student grade entry. 
 * Do not modify this class.
 * 
 * @author Yu-Cheng Tu
 *
 */
public class StudentGrade {
	private int id;
	private String surname;
	private String firstName;
	
	double lab1;
	double lab2;
	double lab3;
	double test;
	double exam;
	
	private HonourClass honours;
	
	/**
	 * Constructor for each student grade entry.
	 * 
	 * @param id Id of the student
	 * @param surname Surname of the student
	 * @param firstName First name of the student
	 */
	public StudentGrade(int id, String surname, String firstName) {
		this.id = id;
		this.surname = surname;
		this.firstName = firstName;
	}
	
	/**
	 * 
	 * @return The final marks for the student
	 */
	public double getFinalMarks() {
		return getCourseworkMarks() + exam/100*60;
	}
	
	/**
	 * 
	 * @return The coursework marks for the student
	 */
	public double getCourseworkMarks() {
		double labTotal = lab1 + lab2 + lab3;
		return (labTotal/300 * 20 + test/100*20);
	}
	
	/**
	 * Sets the final grade for the student
	 * @param honours Honours for the student
	 */
	public void setHonours(HonourClass honours) {
		this.honours = honours;
	}
	
	/**
	 * 
	 * @return The honours for the student
	 */
	public HonourClass getHonours() {
		return honours;
	}
	
	/**
	 * 
	 * @return The surnmae for the student
	 */
	public String getSurname() {
		return surname;
	}
}
