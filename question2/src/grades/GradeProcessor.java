package grades;

import java.io.*;
import java.util.*;

/**
 * This class imports the student grades from a file and calculates the final grades for students.
 * 
 * @author Write your UPI here
 *
 */
public class GradeProcessor {

	// TODO step c) i. Modify this method so that it handles the exception appropriately.
	private static double convertMarks(String marks) throws InvalidDataFormatException {

			if (marks.length() == 0) {
				throw new InvalidDataFormatException();
			}
			return Double.parseDouble(marks);
	}

	// TODO step c) ii. Modify this method to handle the exception appropriately.
	// This methods takes a student grade entry (in the form of a string array) and
	// returns a StudentGrade object.
	private static StudentGrade processStudentGrade(String[] studentGrade) {
		StudentGrade sg = null;
		
		int id = Integer.parseInt(studentGrade[0]);
		String surname = studentGrade[1];
		String firstname = studentGrade[2];

		sg = new StudentGrade(id, surname, firstname);

		try {
			sg.lab1 = convertMarks(studentGrade[3]);
			sg.lab2 = convertMarks(studentGrade[4]);
			sg.lab3 = convertMarks(studentGrade[5]);
			sg.test = convertMarks(studentGrade[6]);
			sg.exam = convertMarks(studentGrade[7]);
		} catch (InvalidDataFormatException e) {
			sg.setHonours(determineHonours(0));
		} //catch (NumberFormatException e){
//			sg.setHonours(determineHonours(0));
//		}

		sg.setHonours(determineHonours(sg.getFinalMarks()));
		
		return sg;
	}

	// TODO step c) iii. Complete this method so that
	// it returns the corresponding honours based on the given final marks.
	private static HonourClass determineHonours(double finalMarks) {
		if(finalMarks>=90){
			return HonourClass.FIRST;
		} else if (finalMarks>=80){
			return HonourClass.SECOND_FIRST;
		} else if (finalMarks>=65){
			return HonourClass.SECOND_SECOND;
		}
		return HonourClass.NA;
	}
		
	// TODO step d) Complete this method so that it returns a set of StudentGrade objects
	// after importing and processing each student grade entry from a file.
	public static Set<StudentGrade> importStudentGrades(String fileName) {
        Set studentGrades = new HashSet();

	    try(BufferedReader br = new BufferedReader(new FileReader(new File(fileName)))){
	        String line;
	        //get rid of the first two header lines.
	        line=br.readLine();
	        line=br.readLine();
	        while ((line = br.readLine())!=null){
	            /*split the line into a string array, use to create new
	            student grade object, iff null then continue.*/
	            String[] gradeArray = line.split(",");
	            StudentGrade sg = processStudentGrade(gradeArray);
	            if(sg==null){
	                continue;
                }

	            studentGrades.add(sg);
            }

        } catch (FileNotFoundException e){
	        System.out.println(e);
	        e.printStackTrace();
        } catch (IOException e){
            System.out.println(e);
            e.printStackTrace();
        }

		return studentGrades;
	}

}
