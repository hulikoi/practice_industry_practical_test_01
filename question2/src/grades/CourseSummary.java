package grades;

import java.util.*;

/**
 * This class provides the summary of grades for a course.
 * 
 * @author Yu-Cheng Tu
 *
 */
public class CourseSummary {
	// Stores the student grades after imported from a file
	private List<StudentGrade> grades;
	
	/**
	 * Constructor.
	 * 
	 * @param inputFileName The file name for importing the student grades
	 */
	public CourseSummary(String inputFileName) {
		grades = new ArrayList<StudentGrade>(); 
		grades.addAll(GradeProcessor.importStudentGrades(inputFileName));
	}
	
	/**
	 * This method prints the average final marks for the course.
	 */
	public void printStudentAverage() {
		double totalScore = 0;
		for (StudentGrade s: grades) {
			totalScore += s.getFinalMarks();
		}
		System.out.printf("The average for the class is %.2f%n", totalScore/grades.size());
	}
	
	/**
	 * This method prints the number of people with the given honours.
	 * 
	 * @param honours Honours for the course
	 */
	public void printCountByHonourClass(HonourClass honours) {

		int count = 0;
		for (StudentGrade s: grades) {
			if (s.getHonours().equals(honours)) {
				count++;
			}
		}
		String honourStr = getHonourString(honours);
		System.out.println("The number of people who have " + honourStr + " is: " + count);
	}
	
	// Helper method for converting each constant in HonourClass to an appropriate String
	private String getHonourString(HonourClass honours) {
		switch (honours) {
			case FIRST:
				return "first class honours";
			case SECOND_FIRST:
				return "second class first division honours";
			case SECOND_SECOND:
				return "second class second division honours";
			default:
				return "no honours";
		}
	}


		
	// TODO step e)
	public void printFirst15InOrder() {
		List first15=new ArrayList<StudentGrade>();
		/*at the moment this just prints out the first 15 students,
		not ordered by surname. there must be an easy way to do this
		but I'm mentally blocked from seeing it.
		*/

		first15.addAll(grades);


		int i =0;
		while (i<15){
			StudentGrade sg=(StudentGrade) first15.get(i);
//			System.out.println(((TreeSet) first15).pollFirst().toString());
			System.out.printf("%s, %.2f, %s%n",sg.getSurname(),sg.getFinalMarks(),getHonourString(sg.getHonours()));
			i++;
		}

	}
	
}
