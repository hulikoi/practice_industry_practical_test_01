package spellchecker;

import com.sun.org.apache.xerces.internal.xs.StringList;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This class is the simple spell checker, which uses a dictionary to check any words passed in from the constructor.
 * 
 * @author Write your UPI here
 *
 */
public class SimpleSpellChecker {

	private Map<String, Integer> userWords; // the words from a user 
	private IDictionary dictionary; // the dictionary to use for spelling check

	// TODO step b) i and ii. Complete the constructor so that
	/* the dictionary for the class is assigned to the one from the
	IDictionary parameter of the constructor, and the userWords
	for the class stores the words to check from the string parameter
	of the constructor.
	*/
	public SimpleSpellChecker(IDictionary dictionary, String wordsToCheck) {
		this.dictionary=dictionary;
		this.userWords=new TreeMap<>();
		//splitting the string into a string array
		String[] words = wordsToCheck.split("[\\s\\W]");
		// taking the array, turning it into a stream, collecting the stream output
        // by type (ie by words that are the same) and then putting the word, plus the
        // size of the collection into the map as key:value pairs.
		Arrays.stream(words)
				.collect(Collectors.groupingBy(s -> s))
				.forEach((k,v)-> userWords.put(k,v.size()));

	}
	
	// TODO step b) iii. Complete this method to return a list of possible
	//  misspelled words.
	public List<String> getMisspelledWords() {
	    //declare and initialise a new list to hold the missspelled words
		List missSpelled = new ArrayList<StringList>();
		//initialise an iterator to go through the Map holding all our words
		Iterator it=userWords.entrySet().iterator();
//iterator is like a stream, keeps going to the end. assigning each map entery (key:value pair)
        // to a variable of type Map.Entry. can use the .getKey and .getvalue methods on this.
        // recall our word is the key not the value.
		while (it.hasNext()){
			Map.Entry wordPair=(Map.Entry)it.next();
			String word = (String) wordPair.getKey();
            //go through each word in the map using the iterator and check with the dictionary.
			if(!dictionary.isSpellingCorrect(word)){
				missSpelled.add(word);
			}
		}
		return missSpelled;
	}
	
	// TODO step b) iv. Complete this method to print a list of
	//  unique words from the userWords map.
	//  The words should be printed in alphabetical order.
	public void printUniqueWords() {
		// ok once you work out that a TreeMap sorts on natural order (alphabetical)
		// by key then you can use the .keySet method here. I assume being a
		// //"set" it's unique values only
		for (String word:userWords.keySet()
			 ) {
			System.out.println(word);
		}
	}
	
	// TODO step b) v. Complete this method to return the
	//  frequency of a given occurring from the userWords map.
	public int getFrequencyOfWord(String word) {
		if(userWords.containsKey(word)){
			return userWords.get(word);
		}
		return 0;
	}
}
