package spellchecker;

import java.util.Set;
import java.util.TreeSet;

public class Dictionary implements IDictionary {
    private Set<String> dictionary;

    public Dictionary() {
        this.dictionary = new TreeSet<String>();
        String[] splitWords = WORDS.split(",");
        for(int i = 0; i < splitWords.length; i++) {
            dictionary.add(splitWords[i]);
        }
    }

    // TODOne step a) i
    @Override
    public boolean isSpellingCorrect(String word) {

        return dictionary.contains(word);
    }
}
