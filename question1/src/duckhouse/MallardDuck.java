package duckhouse;

/**
 * This class defines a Red Head Duck. A duck can fly and quack. It may also lay some eggs.
 *
 * @author Yu-Cheng Tu
 */
public class MallardDuck extends Duck {

    public MallardDuck(String name) {
        this.name = name;
        flyBehaviour = new FlyWithWings();
        quackBehaviour = new Quack();
    }

    // TODOne Step ci. Modify this method.
    @Override
    public int layEggs() {
        return 5;
    }

    // TODOne Step cii. Modify this method.
    @Override
    public String getDuckGreetings() {
        return name+" the Mallard Duck says "+doQuack();
    }

}
