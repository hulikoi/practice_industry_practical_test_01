package duckhouse;

/**
 * This class generates different ducks and displays the information to the console.
 *
 */
public class DuckGenerator {

    private final String[] DUCK_NAMES = {"Sam", "Mary", "George", "David", "Helen",
            "Eve", "Mike", "Kate", "Jessica", "Peter"};

    private Duck[] ducks;
private static final int RED_HEAD_DUCK =1,
                         MALLARD_DUCK=2,
                         RUBBER_DUCK=3;

    // This is the main method. Do not change this!
    public static void main(String[] args) {
        DuckGenerator duckGenerator = new DuckGenerator();
        duckGenerator.start();
    }

    // This is your starting method. Do not change this!
    public void start() {

        // TODOne Step fi. Create the method generateDucks randomly generate 10 ducks of different types.
        ducks = generateDucks();

        System.out.println("Greetings from Ducks");
        System.out.println("====================");

        // TODOnbe Step fii. Create the method printDuckGreetings
        printDuckGreetings();

        System.out.println("====================");

        System.out.println("We are plastic ducks!");
        System.out.println("---------------------");

        // TODOne Step fiii. Create the method printPlasticDucks
        printPlasticDucks();
        System.out.println("--------------------");
    }

    private void printPlasticDucks() {
        for (int i = 0; i < ducks.length; i++) {
            Duck ducky = ducks[i];
            if(ducky instanceof RubberDuck){
                ((RubberDuck) ducky).setMaterial("Plastic");
                System.out.println(ducky.getDuckGreetings() + "i am made of "+((RubberDuck) ducky).getMaterial());
            }
        }
    }

    private void printDuckGreetings() {
        for (int i = 0; i < ducks.length; i++) {
            System.out.println(ducks[i].getDuckGreetings()+" "+ducks[i].doFly());
        }
    }

    private Duck[] generateDucks() {
        ducks = new Duck[10];

        for (int i = 0; i < ducks.length; i++) {
            int duckType=(int)(Math.random()*3)+1;

            if(duckType==RED_HEAD_DUCK){
                ducks[i]=new RedHeadDuck(DUCK_NAMES[i]);
            } else if (duckType==MALLARD_DUCK){
                ducks[i]=new MallardDuck(DUCK_NAMES[i]);
            } else if (duckType==RUBBER_DUCK){
                ducks[i]=new RubberDuck(DUCK_NAMES[i]);
            }

        }
        return ducks;
    }


}
